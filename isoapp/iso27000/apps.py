from django.apps import AppConfig


class Iso27000Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'iso27000'
