from django.contrib import admin
from .models import Category, Document

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','name', 'code')

class DocumentAdmin(admin.ModelAdmin):
    list_display = ('id','name')

admin.site.register(Category, CategoryAdmin)
admin.site.register(Document, DocumentAdmin)
