from django.contrib.auth.models import AbstractUser, Permission, User
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.db import models
from django.core.files import File
from django.urls import reverse
from decimal import Decimal


KIND_CATEGORIES = [
    ('root', 'Raiz'),
    ('view', 'Vista'),
    ('normal', 'Normal')
]
STATES = [
    ('draft', 'Borrador'),
    ('completed', 'Terminado'),
    ('review', 'Revision'),
    ('published', 'Publicado')
]
def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return instance.categorie.name + '/' + filename

class Node(models.Model):
    """docstring for Node."""
    name = models.CharField('Nombre', max_length=150)
    kind = models.CharField('Clase', choices=KIND_CATEGORIES, max_length=6, default='normal')
    code = models.CharField('Codigo', max_length=15)


    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Category(Node):
    categorie = models.ForeignKey('self',blank=True, null=True, related_name="categorias",on_delete=models.PROTECT)

    class Meta:
        verbose_name = ('Categoria')
        verbose_name_plural = ('Categorias')

    def get_absolute_url(self):
        return reverse('category')

    def clean(self):
        # Don't allow draft entries to have a pub_date.
        # if self.status == 'draft' and self.pub_date is not None:
        #     raise ValidationError({'pub_date': _('Draft entries may not have a publication date.')})
        pass

    def delete(self):
        pass

    @property
    def percent_completed_category(self):
        res = 0.0
        if self.kind == 'normal':
            documents = Document.objects.filter(categorie = self)
            lst = [d.percent_completed for d in documents]
            try:
                res =  round(sum(lst)/len(lst), 2)
            except :
                pass
        else:
            categories = Category.objects.filter(categorie = self)
            lst = [Decimal(c.percent_completed_category) for c in categories]
            try:
                res =  round(sum(lst)/len(lst), 2)
            except :
                pass
        # print(sum(res)/len(res))
        return res


class Document(models.Model):
    """docstring for Document."""
    name = models.CharField('Name', max_length=150)
    categorie = models.ForeignKey(Category, related_name='+', on_delete= models.CASCADE)
    version = models.CharField('Version', max_length=10)
    objective = models.TextField('Objetivo', max_length=400, null=True, blank=True)
    comment = models.TextField('Observaciones', max_length=500, null=True, blank=True)
    state = models.CharField('Estado',choices=STATES, default='draft', max_length=20)
    created_by = models.ForeignKey(User, blank=True, null=True, related_name='+', on_delete=models.PROTECT)
    updated_by = models.ForeignKey(User, blank=True, null=True, related_name='+', on_delete=models.PROTECT)
    reviewed_by = models.ForeignKey(User, blank=True, null=True, related_name='+', on_delete=models.PROTECT)
    published_by = models.ForeignKey(User, blank=True, null=True, related_name='+',on_delete=models.PROTECT)
    file = models.FileField(upload_to=user_directory_path)
    percent_completed = models.DecimalField('% de Cumplimiento', max_digits=4, decimal_places=2, default=0.0)

    def get_absolute_url(self):
        return reverse('document')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def delete(self, *args, **kwargs):
        print(args, kwargs)
        # if self.file:
        #     raise ValidationError({'file': ('Can not delete record this have file ')})
        super().delete(*args, **kwargs)  # Call the "real" save() method.

    class Meta:
        verbose_name = ('Documento')
        verbose_name_plural = ('Documentos')
