from django.shortcuts import render
# Create your views here.
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

import mimetypes
import os

def index(request):
    return render(request, 'index.html')

def login_view(request):
    template = 'sb_admin_404.html'
    user = request.user
    if request.method == 'GET':
        template = 'registration/login.html'
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            template = 'dashboard.html'
        # Redirect to a success page.
        elif not request.user.is_anonymous:
            template = 'dashboard.html'
    return render(request, template)

def page_404(request):
    return render(request, 'sb_admin_404.html')

def logout_view(request):
    logout(request)
    return render(request, 'index.html')

@login_required()
def dashboard_view(request):
    template = 'dashboard.html'

    return render(request, template, {})


# @login_required
# def dashboard_view(request):
#     template = 'sb_admin_404.html'
#     user = request.user
#     if request.method == 'POST':
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#     print('ingresa por esta funcion', request.user)
#     template = 'dashboard.html'
#             # Redirect to a success page.
#     elif not request.user.is_anonymous:
#         template = 'dashboard.html'
#     return render(request, template, {})

# def download_file(request, filepath, filename):
#     # # Define Django project base directory
#     # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#     # # Define text file name
#     # filename = 'test.txt'
#     # # Define the full file path
#     # filepath = BASE_DIR + '/downloadapp/Files/' + filename
#     # # Open the file for reading content
#     print('ingresa por aca', filepath, filename)
#     path = open(filepath, 'r')
#     # Set the mime type
#     mime_type, _ = mimetypes.guess_type(filepath)
#     # Set the return value of the HttpResponse
#     response = HttpResponse(path, content_type=mime_type)
#     # Set the HTTP header for sending to browser
#     response['Content-Disposition'] = "attachment; filename=%s" % filename
#     # Return the response value
#     return response
