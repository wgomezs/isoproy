from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Row, Column
from .models import Category, Document
from .models import KIND_CATEGORIES, STATES


class CategoryForm(forms.ModelForm):

    class Meta:
        model= Category
        fields = [
            'name',
            'kind',
            'code',
            'categorie',
        ]

        # labels = {'percent_completed_category': 'Cumplimiento'}

        widgets = {
            'kind': forms.Select(attrs={'class':'form-control'}),
            'categorie': forms.Select(attrs={'class':'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print(args, kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('save_category', 'Guardar'))
        self.helper.add_input(Submit('cancel', 'Cancelar', css_class='btn btn-danger'))

        self.helper.layout = Layout(
            Row(
                Column('name'),
                Column('kind')
            ),
            Row(
                Column('code'),
                Column('categorie')
            ),


        )


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields= [
        'name',
        'version',
        'state',
        'categorie',
        'file'  ,
        'percent_completed',
        'objective',
        'comment'
        ]

        labels={
        'name':'Nombre',
        'version':'Version',
        'state':'Estado',
        'categorie':'Categoria',
        'file':'File',
        'percent_completed': '% de Cumplimiento',
        }

        widgets= {
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'version':forms.TextInput(attrs={'class':'form-control'}),
            'state':forms.Select(attrs={'class':'form-control'}),
            'categorie':forms.Select(attrs={'class':'form-control'}),
            'objective':forms.Textarea(attrs={'class':'form-control', 'rows':'3'}),
            'comment':forms.Textarea(attrs={'class':'form-control', 'rows':'3'}),
            # 'file':forms.FileField(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('save_document', 'Guardar'))
        self.helper.add_input(Submit('cancel', 'Cancelar', css_class='btn btn-danger'))

        self.helper.layout = Layout(
            Row(
                Column('name'),
                Column('version'),
                Column('categorie'),
            ),
            Row(
            Column('state'),
            Column('percent_completed')
            ),
            Row(
                Column('objective'),
                Column('comment')
            ),
            Row(
                Column('file')
            ),

        )
