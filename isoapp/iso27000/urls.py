from django.urls import path, include, re_path
from django.contrib.auth import views as admin_views
from django.conf.urls.static import static
from django.conf import settings
from django.views.static import serve
from . import views_func, views_class

urlpatterns = [
    path('iso27000/', views_func.dashboard_view, name='dashboard'),
    path('accounts/login/', views_func.login_view, name='login'),
    path('', views_func.logout_view, name='logout'),
    path('page_404/', views_func.page_404, name='page_404'),
    path('django-sb-admin/', include('django_sb_admin.urls')),
    path('iso27000/chart', views_class.CategoryChartView.as_view(), name='chart'),
    path('iso27000/categoria/', views_class.CategoryListView.as_view(), name='category'),
    path('iso27000/crear_categoria/', views_class.CategoryCreateView.as_view(), name='create_category'),
    path('iso27000/eliminar_categoria/<pk>', views_class.CategoryDeleteView.as_view(), name='delete_category'),
    path('iso27000/actualizar_categoria/<pk>', views_class.CategoryUpdateView.as_view(), name='update_category'),
    path('iso27000/documento/', views_class.DocumentListView.as_view(), name='document'),
    path('iso27000/crear_documento/', views_class.DocumentCreateView.as_view(), name='create_document'),
    path('iso27000/eliminar_documento/<pk>', views_class.DocumentDeleteView.as_view(), name='delete_document'),
    path('iso27000/actualizar_documento/<pk>', views_class.DocumentUpdateView.as_view(), name='update_document'),
]

if settings.DEBUG:
    urlpatterns+=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # urlpatterns+=static(settings.MEDIA_URL, document_root=settings.STATIC_MEDIA)
