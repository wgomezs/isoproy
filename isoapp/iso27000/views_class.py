from django.shortcuts import render
# Create your views here.
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
import mimetypes
import os
from django.views.generic.edit import DeleteView
from .models import Category, Document
from .forms import CategoryForm, DocumentForm
from django.views import generic
from highcharts.views.pie import HighChartsPieView


class CategoryCreateView(LoginRequiredMixin, generic.CreateView):
    """docstring for CategorieCreateView."""
    model = Category
    form_class = CategoryForm
    template_name = 'categories/categories_template_create.html'
    success = reverse_lazy('categories/categories_template_list/')

    def form_valid(self, form):
        self.object = form.save(commit=False)

        # self.object.published_by = 'xxxx'
        self.object.save()
        # form.save_m2m() this is for save fields m2m
        return super(CategoryCreateView, self).form_valid(form)



class CategoryUpdateView(generic.UpdateView):
    """docstring for CategorieUpdateView."""
    # permission_required = 'iso27000.change_category'
    model = Category
    form_class = CategoryForm
    template_name =  'categories/categories_template_update.html'
    # success = reverse_lazy('categories/categories_template_list/')

    # def __init__(self, arg):
    #     super(CategorieCreateView, self).__init__()
    #     self.arg = arg

    def form_valid(self, form):
        self.object = form.save(commit=False)

        print(self, form)
        # self.object.published_by = 'xxxx'
        self.object.save()
        return super(CategoryUpdateView, self).form_valid(form)


class CategoryListView(generic.ListView):
    model = Category
    context_object_name = 'list_categories'   # your own name for the list as a template variable
    # queryset = Category.objects.order_by('code')
    # paginate_by = 20
    template_name = 'categories/categories_template_list.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        # context['book_list'] = Book.objects.all()
        context['model'] = 'category'
        return context

class CategoryDeleteView(DeleteView):
    """docstring for CategoryDeleteView."""
    model = Category
    template_name = 'categories/categories_template_confirm_delete.html'
    success_url = 'category'


class DocumentListView(generic.ListView):
    model = Document
    context_object_name = 'list_documents'   # your own name for the list as a template variable
    # queryset = Document.objects.order_by('categorie') # Get 5 books containing the title war
    template_name = 'documents/documents_template_list.html'


    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        # context['book_list'] = Book.objects.all()
        context['model'] = 'document'
        return context

class DocumentCreateView(generic.CreateView):
    """docstring for DocumentCreateView."""
    model = Document
    form_class = DocumentForm
    template_name = 'documents/document_template_create.html'


    def form_valid(self, form):
        self.object = form.save(commit=False)
        # self.object.published_by = 'xxxx'
        self.object.save()
        return super(DocumentCreateView, self).form_valid(form)

class DocumentUpdateView(generic.UpdateView):
    """docstring for DocumentUpdateView."""
    model = Document
    form_class = DocumentForm
    template_name = 'documents/document_template_update.html'
    success = reverse_lazy('documents/document_template_list/')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # self.object.published_by = 'xxxx'
        self.object.save()
        return super(DocumentUpdateView, self).form_valid(form)

class DocumentDeleteView(DeleteView):
    """docstring for DocumentUpdateView."""
    model = Document
    template_name = 'documents/documents_template_confirm_delete.html'
    success_url = 'document'

class CategoryChartView(HighChartsPieView):
    # template_name = 'dashboard.html'
    title = 'SGSI'
    subtitle = 'Sistema Gestion de Seguridad de la Informacion'
    exporting = {
        'chartOptions': {
            'plotOptions': {
                'series': {
                    'dataLabels': {
                        'enabled': True
                    }
                }
            }
        }
    }


    @property
    def series(self):
        root, = Category.objects.filter(kind = 'root')
        categories = Category.objects.filter(categorie = root)
        pend = 100
        data = []
        for d in categories:
            pend -= float(d.percent_completed_category)
            value = {
                'name': d.name,
                'y': float(d.percent_completed_category),
                }
            if d.kind == 'view':
                value['drilldown'] = d.code

            data.append(value)
        data.append({'name': 'PENDIENTE', 'y': pend})

        series = [{
                'name': 'SGSI',
                'colorByPoint': 'true',
                'data': data}]
        return series

    def get_drilldown_serie(self, c):
        categories = Category.objects.filter(categorie = c)
        pend2 = 100
        dic_serie = {
            'id': c.code,
            'name': c.name,
            'data': []
        }
        for d in categories:
            pend2 -= float(d.percent_completed_category)
            dic_serie['data'].append([d.name, float(d.percent_completed_category)])
        dic_serie['data'].append(['PENDIENTE', pend2])
        return dic_serie

    @property
    def drilldown(self):
        root, = Category.objects.filter(kind = 'root')
        categories = Category.objects.filter(categorie = root)
        drilldown = {
            'series': []
        }
        for c in categories:
            dic_serie = self.get_drilldown_serie(c)
            drilldown['series'].append(dic_serie)
        return drilldown
